package es.remember.adapter;

import android.content.Context;
import android.database.Cursor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CursorAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import es.remember.db.MyDBHelper;
import es.remember.me.R;

public class RemindAdapter extends CursorAdapter {

	public RemindAdapter(Context context, Cursor reminders) {
		super(context, reminders);

	}

	@Override
	public void bindView(View view, Context context, Cursor cursor) {

		TextView tt = (TextView) view.findViewById(R.id.toptext);
		ImageView iv = (ImageView) view.findViewById(R.id.icon);
		TextView bt = (TextView) view.findViewById(R.id.bottomtext);
		CheckBox cb = (CheckBox) view.findViewById(R.id.checkBox1);

		if (iv != null) {

			// String nombreImagen = Environment.getExternalStorageDirectory() +
			// File.separator +
			// cursor.getString(cursor.getColumnIndex(MyDBHelper.COLUMN_IMAGE));

			// Uri fileUri = Uri.fromFile(new File(nombreImagen));

			// iv.setImageURI(fileUri);
		}

		tt.setText(cursor.getString(cursor
				.getColumnIndex(MyDBHelper.COLUMN_REMINDER)));

		bt.setText(cursor.getString(cursor
				.getColumnIndex(MyDBHelper.COLUMN_LOCATION)));

		if (cb != null) {

			int check = cursor.getInt(cursor
					.getColumnIndex(MyDBHelper.COLUMN_ACTIVE));
			if (check > 0) {
				cb.setChecked(true);
			} else {
				cb.setChecked(false);
			}
		}

	}

	@Override
	public View newView(Context context, Cursor cursor, ViewGroup parent) {
		LayoutInflater inflater = LayoutInflater.from(context);
		View v = inflater.inflate(R.layout.fila, parent, false);
		bindView(v, context, cursor);
		return v;
	}

}
