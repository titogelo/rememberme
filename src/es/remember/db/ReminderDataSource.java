package es.remember.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

public class ReminderDataSource {
	
	private static int ACTIVO = 1;
	
	/**
	 * Referencia para manejar la base de datos. Este objeto lo obtenemos a partir de <code>MyDBHelper</code> y nos proporciona metodos para hacer operaciones
	 * CRUD (create, read, update and delete)
	 */
	private SQLiteDatabase database;
	/**
	 * Referencia al helper que se encarga de crear y actualizar la base de datos.
	 */
	private MyDBHelper dbHelper;
	/**
	 * Columnas de la tabla
	 */
	private String[] allColumns = { MyDBHelper.COLUMN_ID, MyDBHelper.COLUMN_REMINDER,
			MyDBHelper.COLUMN_LOCATION, MyDBHelper.COLUMN_IMAGE, MyDBHelper.COLUMN_ACTIVE, MyDBHelper.COLUMN_LAT, MyDBHelper.COLUMN_LONG};
	
	/**
	 * Constructor.
	 * 
	 * @param context
	 */
	public ReminderDataSource(Context context) {
		dbHelper = new MyDBHelper(context);
	}
	
	/**
	 * Abre una conexion para escritura con la base de datos. Esto lo hace a traves del helper con la llamada a <code>getWritableDatabase</code>. Si la base de
	 * datos no esta creada, el helper se encargara de llamar a <code>onCreate</code>
	 * 
	 * @throws SQLException
	 */
	public void open() throws SQLException {
		database = dbHelper.getWritableDatabase();
	}

	/**
	 * Cierra la conexion con la base de datos
	 */
	public void close() {
		dbHelper.close();
	}
	
	/**
	 * A�ade un nuevo recordatorio a la tabla. Devolvera el identificador obtenido en la consulta.
	 * 
	 * @param recordatorio el texto del recordatorio
	 * @param longitud la longitud de la coordenada
	 * @param latitud la latitud de la coordenada
	 * @param localizacion nombre del lugar en el que se avisara al usuario
	 * 
	 * @return identificador obtenido en la consulta.
	 */
	public long createReminder(String recordatorio, double longitud, double latitud, String localizacion, String imagen) {

		ContentValues values = new ContentValues();
		values.put(MyDBHelper.COLUMN_IMAGE, imagen);
		values.put(MyDBHelper.COLUMN_LOCATION, localizacion);
		values.put(MyDBHelper.COLUMN_REMINDER, recordatorio);
		values.put(MyDBHelper.COLUMN_ACTIVE, ACTIVO);
		values.put(MyDBHelper.COLUMN_LONG, longitud);
		values.put(MyDBHelper.COLUMN_LAT, latitud);

		return database.insert(MyDBHelper.TABLE_REMINDERS, null, values);
	}
	
	/**
	 * Obtiene todas los recordatorios del usuario.
	 * 
	 * @return Lista de objetos de tipo <code>Reminder</code>
	 */
	public Cursor fecthAllReminders(){
		Cursor cursor = database.query(MyDBHelper.TABLE_REMINDERS, allColumns,
		  null, null, null, null, null);
		return cursor;
	}
	
	/**
	 * Obtiene todas los recordatorios del usuario.
	 * 
	 * @return Lista de objetos de tipo <code>Reminder</code>
	 */
	public Cursor fecthReminders(String args[]){
		Cursor cursor = database.query(MyDBHelper.TABLE_REMINDERS, allColumns,
		  "active = ?", args, null, null, null);
		return cursor;
	}
	
	/**
	 * Obtiene un recordatorio del usuario.
	 * 
	 * @return Objeto de tipo <code>Reminder</code>
	 */
	public Cursor fecthReminder(long idReminder) {
		// Lista que almacenara el resultado
		
		Cursor cursor = database.query(MyDBHelper.TABLE_REMINDERS, allColumns,
				MyDBHelper.COLUMN_ID+"="+idReminder, null, null, null, null);
		
		return cursor;
	}
	
	/**
	 * Actualizar un recordatorio
	 * 
	 * @param remindToUpdate
	 *            Objeto de tipo <code>Reminder</code> que se actualizara en bd
	 * 
	 * @return identificador obtenido en la modificacion.
	 */
	public long updateReminder(long id, String recordatorio, double longitud, double latitud, String localizacion, String imagen, boolean activo) {
		ContentValues values = new ContentValues();
		
		values.put(MyDBHelper.COLUMN_IMAGE, imagen);
		values.put(MyDBHelper.COLUMN_LOCATION, localizacion);
		values.put(MyDBHelper.COLUMN_REMINDER, recordatorio);
		values.put(MyDBHelper.COLUMN_ACTIVE, activo);
		values.put(MyDBHelper.COLUMN_LONG, longitud);
		values.put(MyDBHelper.COLUMN_LAT, latitud);

		return database.update(MyDBHelper.TABLE_REMINDERS, values, "_id="+id, null);
	}
	
	/**
	 * Desactiva un recordatorio
	 * 
	 * @param id identificador del reminder
	 * 		  activo boolean para cambiar el estado del reminder
	 * 
	 * @return identificador obtenido en la modificacion.
	 */
	public long setUnsetReminder(long id, boolean activo) {
		ContentValues values = new ContentValues();
		
		values.put(MyDBHelper.COLUMN_ACTIVE, activo);
		
		return database.update(MyDBHelper.TABLE_REMINDERS, values, "_id="+id, null);
	}
	
	/**
	 * Eliminar un recordatorio
	 * 
	 * @param remindToDelete
	 *            Objeto de tipo <code>Reminder</code> que se borrara en bd
	 * 
	 * @return identificador obtenido en el borrado.
	 */
	public void deleteReminder(long id) {

		database.delete(MyDBHelper.TABLE_REMINDERS, "_id="+id, null);
		
	}
	
	/**
	 * Eliminar todos los recordatorios
	 * 
	 */
	public void deleteAllReminders() {

		database.delete(MyDBHelper.TABLE_REMINDERS, "", null);
		
	}
	
}
