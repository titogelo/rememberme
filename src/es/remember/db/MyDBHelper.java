package es.remember.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class MyDBHelper extends SQLiteOpenHelper {

	/**
	 * Nombre de la tabla valorations y sus columnas
	 */
	public static final String TABLE_REMINDERS = "reminders";
	/**
	 * Columna _id de la tabla. Es conveniente tenerlo (uso de contentprovider) aunque no obligatorio
	 */
	public static final String COLUMN_ID = "_id";
	public static final String COLUMN_REMINDER = "reminder";
	public static final String COLUMN_LOCATION = "location";
	public static final String COLUMN_ACTIVE = "active";	
	public static final String COLUMN_IMAGE = "image";
	public static final String COLUMN_LONG = "longitud";
	public static final String COLUMN_LAT = "latitud";

	/**
	 * Nombre y version de la base de datos
	 */
	private static final String DATABASE_NAME = "reminders.db";
	private static final int DATABASE_VERSION = 1;
	
	/**
	 * Script para crear la base datos
	 */
	private static final String DATABASE_CREATE = "create table " + TABLE_REMINDERS + "( " + COLUMN_ID +
	 " integer primary key autoincrement, " 
	 + COLUMN_REMINDER+ " text, "
	 + COLUMN_IMAGE + " text, " 
	 + COLUMN_LOCATION + " text, "
	 + COLUMN_LONG + " decimal, "
	 + COLUMN_LAT + " decimal, "
	 + COLUMN_ACTIVE +" boolean );";

	/**
	 * Script para borrar la base de datos
	 */
	private static final String DATABASE_DROP = "DROP TABLE IF EXISTS " + TABLE_REMINDERS;

	
	public MyDBHelper(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		// TODO Auto-generated method stub
		db.execSQL(DATABASE_CREATE);
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		// TODO Auto-generated method stub
		Log.w("UPDATE", "updating database from version " + oldVersion + " to version " + newVersion);
		// Al ser un ejemplo, lo que hacemos al actualizar la version es borrar
		// la base de datos y volver a crearla.
		// Quiza esto no sea lo mas eficiente, ya que normalmente deberemos
		// conservar los datos almacenados, por lo que deberiamos hacer un
		// volcado de la base de datos, y una vez terminada la actualizacion,
		// proceder a restaurarlos
		db.execSQL(DATABASE_DROP);
		this.onCreate(db);
	}

}
