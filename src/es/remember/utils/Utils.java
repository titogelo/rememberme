package es.remember.utils;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

public class Utils {

	public void muestraMensaje(Context context, String texto) {
		Toast.makeText(context, texto, Toast.LENGTH_LONG)
				.show();
	}

	public void registraLog(String titulo, String descripcion) {
		Log.i(titulo, descripcion);
	}
	
	public void registraLogDebug(String titulo, String descripcion) {
		Log.d(titulo, descripcion);
	}
	
}
