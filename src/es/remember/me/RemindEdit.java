package es.remember.me;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ToggleButton;
import es.remember.db.MyDBHelper;
import es.remember.db.Reminder;
import es.remember.db.ReminderDataSource;
import es.remember.utils.Utils;

public class RemindEdit extends Activity {

	private EditText reminderEdit;

	private ImageView reminderImage;

	private String destino = "";
	private Double longitud = 0.0;
	private Double latitud = 0.0;
	private boolean reminderActivo;
	private Button boton_mapa;
	private Button boton_camara;
	private CheckBox camaraCheck;
	private CheckBox mapaCheck;
	private ToggleButton toggleActivo;
	private Long mRowId = null;

	private Uri fileUri = null;

	private String nombreImagen = "";
	private String photoFile = "";

	private static Utils utilidades;

	private Reminder reminder;

	private static final int ACTIVITY_MAP_CREATE = 1;
	private static final int ACTIVITY_CAMERA_CREATE = 2;

	private ReminderDataSource reminderDataSource;

	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.edicion);

		reminderDataSource = new ReminderDataSource(this);
		reminderDataSource.open();

		reminder = new Reminder();
		utilidades = new Utils();
		
		reminderEdit = (EditText) findViewById(R.id.recordatorio);
		reminderImage = (ImageView) findViewById(R.id.vistaPrevia);

		mapaCheck = (CheckBox) findViewById(R.id.mapa_ok);
		camaraCheck = (CheckBox) findViewById(R.id.camara_ok);
		boton_mapa = (Button) findViewById(R.id.boton_obtener_mapa);
		boton_camara = (Button) findViewById(R.id.boton_obtener_camara);
		toggleActivo = (ToggleButton) findViewById(R.id.toggleButton1);

		mRowId = (savedInstanceState == null) ? null
				: (Long) savedInstanceState
						.getSerializable(MyDBHelper.COLUMN_ID);

		if (mRowId == null) {
			Bundle extras = getIntent().getExtras();
			mRowId = extras != null ? extras.getLong(MyDBHelper.COLUMN_ID)
					: null;
		}

		utilidades.registraLog("Edicion", "OnCreate");

		boton_mapa.setOnClickListener(new OnClickListener() {

			public void onClick(View v) {

				Intent intent = new Intent(RemindEdit.this, RemindMap.class);
				intent.putExtra("destino", reminder.getLocation());
				intent.putExtra("latitud", reminder.getLatitud());
				intent.putExtra("longitud", reminder.getLongitud());

				utilidades.registraLog("Envio a RemindMap",
						"Destino:" + reminder.getLocation() + " Latitud: "
								+ reminder.getLatitud() + " Longitud:"
								+ reminder.getLongitud());

				startActivityForResult(intent, ACTIVITY_MAP_CREATE);
			}
		});

		toggleActivo.setOnCheckedChangeListener(new OnCheckedChangeListener() {

			public void onCheckedChanged(CompoundButton buttonView,
					boolean isChecked) {
				reminder.setActive(isChecked);
				reminderActivo = isChecked;
			}
		});

		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyymmddhhmmss");
		String date = dateFormat.format(new Date());
		photoFile = "Recordatorio_" + date + ".jpg";

		nombreImagen = Environment.getExternalStorageDirectory()
				+ File.separator + photoFile;

		boton_camara.setOnClickListener(new OnClickListener() {

			public void onClick(View v) {
				Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

				fileUri = Uri.fromFile(new File(nombreImagen));
				intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);

				startActivityForResult(intent, ACTIVITY_CAMERA_CREATE);
			}
		});
	}

	private void populateFields() {
		if (mRowId != null) {
			Cursor cursorReminder = reminderDataSource.fecthReminder(mRowId);
			startManagingCursor(cursorReminder);
			if (cursorReminder.moveToFirst()) {

				reminderEdit.setText(cursorReminder.getString(cursorReminder
						.getColumnIndexOrThrow(MyDBHelper.COLUMN_REMINDER)));

				reminder.setReminder(cursorReminder.getString(cursorReminder
						.getColumnIndexOrThrow(MyDBHelper.COLUMN_REMINDER)));

				if (destino.equals("")) {
					reminder.setLocation(cursorReminder.getString(cursorReminder
							.getColumnIndexOrThrow(MyDBHelper.COLUMN_LOCATION)));
				} else {
					reminder.setLocation(destino);
				}

				if (latitud == 0) {
					reminder.setLatitud(cursorReminder.getDouble(cursorReminder
							.getColumnIndexOrThrow(MyDBHelper.COLUMN_LAT)));
				} else {
					reminder.setLatitud(latitud);
				}

				if (longitud == 0) {
					reminder.setLongitud(cursorReminder.getDouble(cursorReminder
							.getColumnIndexOrThrow(MyDBHelper.COLUMN_LONG)));
				} else {
					reminder.setLongitud(longitud);

				}

				if (cursorReminder.getInt(cursorReminder
						.getColumnIndex(MyDBHelper.COLUMN_ACTIVE)) == 1) {
					reminder.setActive(true);
					toggleActivo.setChecked(true);
				} else {
					toggleActivo.setChecked(false);
					reminder.setActive(false);
				}

				if (nombreImagen.equals("")) {
					// reminder.setImage(cursorReminder.getString(cursorReminder.getColumnIndexOrThrow(MyDBHelper.COLUMN_IMAGE)));
					// fileUri = Uri.fromFile(new
					// File(cursorReminder.getString(cursorReminder.getColumnIndexOrThrow(MyDBHelper.COLUMN_IMAGE))));
					// reminderImage.setImageURI(fileUri);

				} else {
					// reminder.setImage(nombreImagen);
					// reminderImage.setImageBitmap(BitmapFactory.decodeFile(reminder.getImage()));
				}
			}
		}
	}

	@Override
	protected void onResume() {
		super.onResume();
		utilidades.registraLog("Interno", "Pasa por el onResume");

		populateFields();

	}

	@Override
	protected void onPause() {
		super.onPause();
		utilidades.registraLog("Interno", "Pasa por el onPause");
		saveState();
	}

	@Override
	protected void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		saveState();
		outState.putSerializable(MyDBHelper.COLUMN_ID, mRowId);
	}

	private void saveState() {
		String reminder_text = reminderEdit.getText().toString();
		if (mRowId == null) {

			long id = reminderDataSource.createReminder(reminder_text,
					longitud, latitud, destino, reminder.getImage());

			utilidades.registraLog(
					"Edicion guardar nuevo",
					"Recordatorio:" + reminder_text + " Long:"
							+ reminder.getLongitud() + " Lat:"
							+ reminder.getLatitud() + " Destino:"
							+ reminder.getLocation() + " Imagen:"
							+ reminder.getImage() + " Activo:"
							+ reminder.isActive());

			if (id > 0) {
				mRowId = id;
			}
		} else {

			utilidades.registraLog(
					"Edicion guardar",
					"Recordatorio:" + reminder_text + " Long:"
							+ reminder.getLongitud() + " Lat:"
							+ reminder.getLatitud() + " Destino:"
							+ reminder.getLocation() + " Imagen:"
							+ reminder.getImage() + " Activo:"
							+ reminder.isActive());

			reminderDataSource
					.updateReminder(mRowId, reminder_text,
							reminder.getLongitud(), reminder.getLatitud(),
							reminder.getLocation(), reminder.getImage(),
							reminderActivo);

		}
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (requestCode == ACTIVITY_MAP_CREATE) {

			if (resultCode == RESULT_OK) {
				Bundle extras = data.getExtras();

				latitud = extras.getDouble("latitud");
				longitud = extras.getDouble("longitud");
				destino = extras.getString("destino");

				mapaCheck.setChecked(true);

				utilidades.registraLog("Edicion recordatorio", "Latitud:"
						+ latitud + " Longitud:" + longitud + " Destino:"
						+ destino);

			} else if (resultCode == RESULT_CANCELED) {
				utilidades.registraLog("Edicion recordatorio",
						"No se ha seleccionado destino");
			}

		}

		if (requestCode == ACTIVITY_CAMERA_CREATE) {
			if (resultCode == RESULT_OK) {

				if (data != null) {

					reminder.setImage(data.getData().toString());

					// reminderImage.setImageBitmap(BitmapFactory.decodeFile(data.getData().toString()));

					fileUri = data.getData();

					// reminderImage.setImageBitmap(BitmapFactory.decodeFile(fileUri.toString()));

					reminderImage.setImageURI(fileUri);

					camaraCheck.setChecked(true);

					// utilidades.muestraMensaje(getApplicationContext(),fileUri.toString());
				}

			} else if (resultCode == RESULT_CANCELED) {
				utilidades.muestraMensaje(getApplicationContext(),
						"Foto no sacada");
			}
		}
	}
}
