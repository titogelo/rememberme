package es.remember.me;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.speech.RecognizerIntent;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.maps.GeoPoint;
import com.google.android.maps.MapActivity;
import com.google.android.maps.MapView;

import es.remember.utils.Utils;

public class RemindMap extends MapActivity/* implements SensorEventListener */{

	private EditText destino;
	private Button boton_buscar;
	private Button boton_dictar;
	private MapView mapView;
	private double latitud = 43.355093 * 1E6;
	private double longitud = -5.851805 * 1E6;

	private double latitud_edicion;
	private double longitud_edicion;
	private String destino_edicion;

	private Geocoder geocoder;
	private GeoPoint geoPoint;
	public static final int MAX_RESULTS = 10;

	private Utils utilidades;

	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.mapa);

		mapView = (MapView) findViewById(R.id.mapa);
		boton_buscar = (Button) findViewById(R.id.obtener);
		boton_dictar = (Button) findViewById(R.id.boton_habla_para_buscar_ciudad);

		destino = (EditText) findViewById(R.id.select_direction);

		geocoder = new Geocoder(this);
		utilidades = new Utils();

		boton_buscar.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				geoLocalizar();
			}
		});

		boton_dictar.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				dictarDestino();
			}
		});

		mapView.setBuiltInZoomControls(true);

		populateFields();

		mostrarOpciones();

	}

	@Override
	public boolean dispatchTouchEvent(MotionEvent event) {

		if (event.getAction() == MotionEvent.ACTION_UP) {
			geoPoint = mapView.getProjection().fromPixels((int) (event.getX()),
					(int) (event.getY()));

			Toast.makeText(getApplicationContext(),
					"Latitud: " + geoPoint.getLatitudeE6() / 1E6
							+ " Longitud: " + geoPoint.getLongitudeE6() / 1E6,
					Toast.LENGTH_LONG);

			AlertDialog dialog = new AlertDialog.Builder(this).create();
			dialog.setTitle(getString(R.string.mapa_sel_punto_titulo));
			dialog.setMessage(getString(R.string.mapa_sel_punto_descripcion));

			// Deseo confirmar este punto
			dialog.setButton(getString(R.string.mapa_sel_punto_ok),
					new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog,
								int whichButton) {
							Bundle bundle = new Bundle();
							bundle.putDouble("latitud",
									geoPoint.getLatitudeE6() / 1E6);
							bundle.putDouble("longitud",
									geoPoint.getLongitudeE6() / 1E6);
							bundle.putString("destino", destino.getText()
									.toString());

							Intent resultados = new Intent();
							resultados.putExtras(bundle);

							setResult(RESULT_OK, resultados);

							RemindMap.this.finish();
						}
					});

			// Deseo continuar buscando el destino
			dialog.setButton2(getString(R.string.mapa_sel_punto_no),
					new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog,
								int whichButton) {
							dialog.cancel();
						}
					});

			dialog.show();

		}

		return super.dispatchTouchEvent(event);
	}

	private void mostrarOpciones() {
		AlertDialog dialog = new AlertDialog.Builder(this).create();
		dialog.setTitle(getString(R.string.mapa_elige_opcion));
		dialog.setMessage(getString(R.string.mapa_descripcion_uso));

		dialog.setButton(getString(R.string.mapa_dicte_destino),
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int whichButton) {
						// Obtenemos el destino dictado por el usuario
						dictarDestino();
					}
				});

		dialog.setButton3(getString(R.string.mapa_cancel),
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int whichButton) {
						// Cerramos el cuadro de dialog
						dialog.cancel();
					}
				});

		dialog.show();
	}

	protected void dictarDestino() {
		Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
		intent.putExtra(RecognizerIntent.EXTRA_PROMPT,
				"Android Speech Recognition example");

		startActivityForResult(intent, 0);
	}

	/**
	 * Metodo que geolocaliza el destino que este reflejado en el input de la
	 * parte superior del layout
	 * */
	public void geoLocalizar() {
		try {
			geocoder = new Geocoder(this);
			utilidades.registraLog("Geolocalizacion", "Destino a localizar: "
					+ destino.getText().toString());
			List<Address> addressList = geocoder.getFromLocationName(destino
					.getText().toString(), 1);
			utilidades.registraLog("GeoLocalizacion", "Buscando..."
					+ destino.getText().toString());

			if (addressList != null && addressList.size() > 0) {
				int lat = (int) (addressList.get(0).getLatitude() * 1E6);
				int lng = (int) (addressList.get(0).getLongitude() * 1E6);

				GeoPoint pt = new GeoPoint(lat, lng);

				mapView.getController().setZoom(18);
				mapView.getController().setCenter(pt);
			}

			((InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE))
					.hideSoftInputFromWindow(destino.getWindowToken(), 0);

		} catch (IOException e) {
			Log.e("GeoLocalizacion", "Fallo al geolocalizar..."
					+ destino.getText().toString());
			e.printStackTrace();
		}
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (requestCode == 0) {
			if (resultCode == RESULT_OK) {

				ArrayList<String> matches1 = data
						.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
				destino.setText("" + matches1.get(0));

				// Geolocalizamos el destino seleccionado
				geoLocalizar();

				utilidades.registraLog("Speech To Text","Se ha procesado destino correctamente...");

			} else if (resultCode == RESULT_CANCELED) {
				utilidades.muestraMensaje(getApplicationContext(),"Destino no procesado...");
				utilidades.registraLog("Speech To Text", "No se ha procesado el destino...");
			}
		}
	}

	private void populateFields() {

		Intent intent = getIntent();

		Bundle extras = intent.getExtras();
		latitud_edicion = extras.getDouble("latitud");
		longitud_edicion = extras.getDouble("longitud");
		destino_edicion = extras.getString("destino");

		if (latitud_edicion != 0.0) {
			latitud_edicion *= 1E6;
			latitud = latitud_edicion;
		}

		if (longitud_edicion != 0.0) {
			longitud_edicion *= 1E6;
			longitud = longitud_edicion;
		}

		if (destino_edicion != null) {
			destino.setText(destino_edicion);
		}

		utilidades.registraLog("Datos recibidos en RemindMap", "Destino:"
				+ destino_edicion + " Latitud: " + latitud_edicion
				+ " Longitud:" + longitud_edicion);

		mapView.getController().animateTo(
				new GeoPoint((int) latitud, (int) longitud));
		mapView.getController().setZoom(18);
		mapView.setClickable(true);
		mapView.setFocusable(true);
		mapView.setSatellite(true);

	}

	@Override
	protected boolean isRouteDisplayed() {
		// TODO Auto-generated method stub
		return false;
	}
}
