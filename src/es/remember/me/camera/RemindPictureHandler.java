package es.remember.me.camera;

import java.io.File;
import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;

import android.content.Context;
import android.hardware.Camera;
import android.hardware.Camera.PictureCallback;
import android.os.Environment;
import es.remember.utils.Utils;

public class RemindPictureHandler implements PictureCallback {

	private final Context context;
	private Utils utilidades;
	
	public RemindPictureHandler(Context context){
		this.context = context;
	}
	
	public void onPictureTaken(byte[] data, Camera camera) {
		
		File pictureFileDir = getDir();
		
		if (!pictureFileDir.exists() && !pictureFileDir.mkdirs()) {			
			utilidades.registraLogDebug("Creacion directorio", "No se ha podido crear el directorio para guardar la imagen");
			utilidades.muestraMensaje(context, "No se ha podido crear el directorio para guardar la imagen");
			return;
		}
		
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyymmddhhmmss");
		String date = dateFormat.format(new Date());
		String photoFile = "Recordatorio_"+date+".jpg";
		
		String filename = pictureFileDir.getPath()+ File.separator + photoFile;
		
		File pictureFile = new File(filename);
		
		try{
			FileOutputStream fos = new FileOutputStream(pictureFile);
			fos.write(data);
			fos.close();
			utilidades.registraLog("Capturar imagen", "El fichero "+filename+" ha sido creado correctamente");
			utilidades.muestraMensaje(context, "La imagen "+photoFile+" ha sido guardada");
			
		} catch(Exception error){
			utilidades.registraLogDebug("Capturar imagen", "El fichero "+filename+" no ha sido creado");
			utilidades.muestraMensaje(context, "La imagen no ha podido ser guardada");
		}
		
	}

	private File getDir() {
		File sdDir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
		return new File(sdDir,"RecordatoriosImagenes");
	}

	
}
