package es.remember.me;

import java.util.ArrayList;

import android.app.AlertDialog;
import android.app.ListActivity;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView.AdapterContextMenuInfo;
import android.widget.ListView;
import es.remember.adapter.RemindAdapter;
import es.remember.db.MyDBHelper;
import es.remember.db.ReminderDataSource;
import es.remember.utils.Utils;

public class MainActivity extends ListActivity {

	private ReminderDataSource reminderDataSource;

	private Utils utilidades;

	private float metros = 200;

	public static final int INSERT_ID = Menu.FIRST;
	public static final int DELETE_ID = Menu.FIRST;
	public static final int DELETE_ALL = 3;
	public static final int SETTINGS = 4;
	public static final int PREFERENCES = 5;
	private static final int ACTIVITY_CREATE = 0;
	private static final int ACTIVITY_EDIT = 1;
	private static final int ACTIVITY_SETTINGS = 2;
	private Cursor cursorReminder;

	/**
	 * Se crea al inicio
	 * */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main);

		utilidades = new Utils();

		// Obtengo los valores de las sharedpreferences
		// SharedPreferences pref =
		// PreferenceManager.getDefaultSharedPreferences(this);
		// metros = pref.getFloat("distancia_metros", 200);


		reminderDataSource = new ReminderDataSource(this);
		reminderDataSource.open();
		fillData();
		registerForContextMenu(getListView());

		// Control de aproximaci�n
		controlAproximacion();
	}

	private void controlAproximacion() {

		utilidades.registraLogDebug("MainActivity",
				getString(R.string.preferencias_metros_debug) + metros);

		LocationManager localizador = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

		// Si el GPS no est� habilitado
		if (!localizador.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
			utilidades.muestraMensaje(getApplicationContext(),
					getString(R.string.localizacion_gps_deshabilitado));
		}

		LocationListener locListener = new LocationListener() {

			public void onLocationChanged(Location location) {

				ArrayList<Integer> reminders_activados = new ArrayList<Integer>();

				String[] args = new String[] { "1" };

				cursorReminder = reminderDataSource.fecthReminders(args);

				if (!cursorReminder.moveToFirst()) {
					return;
				}

				// Creamos la localizacion sobre el punto en el que estamos
				Location posicion_actual = new Location("posicion_actual");
				posicion_actual.setLatitude(location.getLatitude());
				posicion_actual.setLongitude(location.getLongitude());

				if (cursorReminder.moveToFirst()) {
					do {
						// Creamos la localizacion sobre el punto que queremos
						// comparar
						Location posicion_a_comparar = new Location(
								"posicion_a_comparar");
						posicion_a_comparar
								.setLatitude(cursorReminder.getDouble(cursorReminder
										.getColumnIndexOrThrow(MyDBHelper.COLUMN_LAT)));
						posicion_a_comparar
								.setLongitude(cursorReminder.getDouble(cursorReminder
										.getColumnIndexOrThrow(MyDBHelper.COLUMN_LONG)));

						double distancia = posicion_actual
								.distanceTo(posicion_a_comparar);

						if (distancia < metros) {
													
							reminders_activados
									.add(cursorReminder.getInt(cursorReminder
											.getColumnIndexOrThrow(MyDBHelper.COLUMN_ID)));
						}
					} while (cursorReminder.moveToNext());
				}

				if (!reminders_activados.isEmpty()) {

					String ns = Context.NOTIFICATION_SERVICE;
					NotificationManager notManager = (NotificationManager) getSystemService(ns);

					long hora = System.currentTimeMillis();

					Notification notificacion = new Notification(
							R.drawable.ic_launcher,
							getString(R.string.notificacion1), hora);

					Intent notIntent = new Intent(getApplicationContext(),
							RemindEdit.class);

					reminderDataSource.setUnsetReminder(new Long(
							reminders_activados.get(0).longValue()), false);

					notIntent.putExtra(MyDBHelper.COLUMN_ID, new Long(
							reminders_activados.get(0).longValue()));

					PendingIntent contIntent = PendingIntent.getActivity(
							getApplicationContext(), 0, notIntent, 0);

					notificacion.setLatestEventInfo(getApplicationContext(),
							getString(R.string.notificacion1),
							getString(R.string.notificacion2), contIntent);

					notificacion.flags |= Notification.FLAG_AUTO_CANCEL;

					notManager.notify(1, notificacion);

					reminders_activados.clear();

				}

			}

			public void onProviderDisabled(String provider) {
				utilidades.muestraMensaje(getApplicationContext(), getString(R.string.main_gps_desactivado));
			}

			public void onProviderEnabled(String provider) {
				//utilidades.muestraMensaje(getApplicationContext(), getString(R.string.main_gps_activado));
			}

			public void onStatusChanged(String provider, int status,Bundle extras) {
				//utilidades.muestraMensaje(getApplicationContext(), getString(R.string.main_gps_cambia_status));
			}

		};

		localizador.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0,
				locListener);
	}

	/**
	 * Metodo que inicializa el menu de contexto
	 * */
	public void onCreateContextMenu(ContextMenu menu, View v,
			ContextMenuInfo menuInfo) {
		super.onCreateContextMenu(menu, v, menuInfo);
		menu.add(0, DELETE_ID, 0, R.string.menu_delete);
	}

	/**
	 * Metodo llamado cuando se selecciona un item en el menu de contexto
	 * */
	public boolean onContextItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case DELETE_ID:
			AdapterContextMenuInfo info = (AdapterContextMenuInfo) item
					.getMenuInfo();
			reminderDataSource.deleteReminder(info.id);
			fillData();

			return true;
		}
		return super.onContextItemSelected(item);
	}

	/**
	 * Metodo que inicializa las opciones del menu
	 * */
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		boolean result = super.onCreateOptionsMenu(menu);
		menu.add(0, INSERT_ID, 0, R.string.options_insert);
		menu.add(0, DELETE_ALL, 1, R.string.options_delete_all);
		menu.add(1, SETTINGS, 2, R.string.options_settings);
		menu.add(1, PREFERENCES, 3, R.string.options_preferences);
		return result;
	}

	/**
	 * Metodo llamado cuando se selecciona un item en el menu de opciones
	 * */
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case INSERT_ID:
			createRemind();
			return true;
		case DELETE_ALL:
			deleteAll();
			return true;
		case SETTINGS:
			goToSetting();
			return true;
		case PREFERENCES:
			Intent intentPreferences = new Intent(MainActivity.this,
					RemindPreferences.class);
			startActivity(intentPreferences);
			return true;
		}

		return super.onOptionsItemSelected(item);
	}

	/**
	 * Metodo que se llama cuando se selecciona un item en el listview
	 * */
	@Override
	protected void onListItemClick(ListView l, View v, int position, long id) {

		super.onListItemClick(l, v, position, id);
		Intent i = new Intent(this, RemindEdit.class);
		i.putExtra(MyDBHelper.COLUMN_ID, id);

		utilidades.registraLog("ListView",
				"El id del recordatorio que modificamos:" + id);

		startActivityForResult(i, ACTIVITY_EDIT);
	}
	
	

	/**
	 * Metodo para controlar la creci�n de un recordatorio nuevo
	 * */
	private void createRemind() {
		Intent intent = new Intent(MainActivity.this, RemindEdit.class);

		startActivityForResult(intent, ACTIVITY_CREATE);

		fillData();
	}

	private void deleteAll() {
		AlertDialog dialog = new AlertDialog.Builder(this).create();
		dialog.setTitle(getString(R.string.main_eliminar_recordatorios));
		dialog.setMessage(getString(R.string.main_eliminar_recordatorios_descripcion));

		// Eliminar todo
		dialog.setButton(getString(R.string.main_eliminar_todo_ok),
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog,
							int whichButton) {
						reminderDataSource.deleteAllReminders();
						fillData();
					}
				});

		// Cancelar
		dialog.setButton2(getString(R.string.main_eliminar_todo_no),
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog,
							int whichButton) {
						dialog.cancel();
					}
				});

		dialog.show();
	}

	private void goToSetting() {
		startActivityForResult(new Intent(
				android.provider.Settings.ACTION_SETTINGS), ACTIVITY_SETTINGS);
	}

	/**
	 * Metodo para rellenar el listview
	 * */
	private void fillData() {
		cursorReminder = reminderDataSource.fecthAllReminders();

		startManagingCursor(cursorReminder);

		RemindAdapter remind_adapter = new RemindAdapter(this, cursorReminder);
		setListAdapter(remind_adapter);
	}

	/**
	 * Metodo que recoge el evento que se genera despues de llamar al metodo
	 * startActivityForResult.
	 * 
	 * Puede recoger un alta o una modificaci�n de un recordatorio.
	 * */
	@Override
	protected void onActivityResult(int requestCode, int resultCode,
			Intent intent) {

		super.onActivityResult(requestCode, resultCode, intent);

		fillData();

	}
}