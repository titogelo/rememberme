package es.remember.me;

import android.app.Activity;
import android.content.pm.PackageManager;
import android.hardware.Camera;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.View;
import es.remember.me.camera.RemindPictureHandler;
import es.remember.utils.Utils;

public class RemindCamera extends Activity {

	private Camera camera;
	private Utils utilidades;
	
	@Override
	public void onCreateContextMenu(ContextMenu menu, View v,
			ContextMenuInfo menuInfo) {
		super.onCreateContextMenu(menu, v, menuInfo);
		
		setContentView(R.layout.camara);
		
		utilidades = new Utils();
		
		if (!getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA)) {
			utilidades.muestraMensaje(getApplicationContext(), getString(R.string.camara_sin_camara_en_dispositivo));
		} else {
			camera = Camera.open();
		}
		
	}

	public void onClick(View view){
		camera.takePicture(null, null, new RemindPictureHandler(getApplicationContext()));
	}
}
